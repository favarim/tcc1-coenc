# Disciplina: Trabalho de Conclusão de Curso 1 (TCC1)
## Curso: Engenharia de Computação - UTFPR - _Campus_ Pato Branco
### Responsável: Prof. Dr. Fábio Favarim
- [Cronograma de Apresentações de TCC1 - 2024/2](tcc1-20242.md)
- [Cronograma de Apresentações de TCC1 - 2024/1](tcc1-20241.md)
- [Cronograma de Apresentações de TCC1 - 2023/2](tcc1-20232.md)

